$(document).ready(function() {
    
    $('#send-email').on('click', function() {
        
        confirm("Send a message?");
    });

    $('.arrow').on('click', 'a', function(event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 3000);
    });
});

